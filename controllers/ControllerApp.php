<?php
class ControllerApp extends BaseController {

	public function index() {
		$this->app->render('main.php');
	}

	public function ajax(){
		$hijri = new HijriCalendar();
		$search = ORM::for_table('party')->raw_query('SELECT *,p.name as party_name, s.id as schedule_id FROM schedule s INNER JOIN party p ON s.party = p.id INNER JOIN mohalla m ON s.mohalla = m.id WHERE p.its = '.$this->app->request->get('search_term'))->find_array();
		$this->app->render('search.php',  array('search_results' => $search, 'hijri_date' => $hijri));
	}

	public function attended() {
		$aResult = array();
		try {
			$params = $this->app->request()->post();
			$oModel = Model::factory('Schedule')->find_one($params['id']);
			if(!$oModel)
				throw new Exception("Error Processing Schedule");
			
			if($params['type'] == "user")
				$oModel->attended = $params['status'];
			
			if($params['type'] == "aamil")
				$oModel->verified = $params['status'];
				
			$oModel->save();
			
			$aResult['success'] = "Attendance Saved";
		} catch (Exception $e) {
			$aResult['error'] = $e->getMessage();
		}
		echo json_encode($aResult);
			
	}

	public function login() {

		$this->app->render('login.php');
	}

	public function authenticate() {
		try {
			$params = $this->app->request->params();
			$oModel = Model::factory('User')->create();
			$aUser = $oModel->authenticate($params);
			if(isset($aUser['error']))
				throw new Exception($aUser['error']);
			
			$redirect = 'mohalla';
		} catch(Exception $e) {
			$this->app->flash('errors', $e->getMessage());
			$redirect = 'aamil';
		}
		$this->app->redirect(CURR_DIR . $redirect);
	}

	public function mohallaIndex() {
		$result['aMohalla'] = CHtml::listData(Model::factory('Mohalla')->order_by_asc('name')->find_many(), 'id', 'name');

		$this->app->render('mohalla.php', $result);
	}

	public function mohallaDetail() {
		$hijri = new HijriCalendar();
		$search = ORM::for_table('party')->raw_query('SELECT *, m.name as mohalla, p.name as party_name , s.id as schedule_id FROM schedule s INNER JOIN party p ON s.party = p.id INNER JOIN mohalla m ON s.mohalla = m.id WHERE s.mohalla = '.$this->app->request->post('search_term'). ' order by s.date ASC')->find_array();
		$status = array('-1' => '-', '0'=> 'No', '1'=>'Yes');
		$this->app->render('mohalla.php',  array('search_results' => $search, 'hijri_date' => $hijri, 'status'=> $status));
	}

	public function party() {

		$partys = ORM::for_table('party')->order_by_asc('zone')->find_array();
		$this->app->render('party.php',  array('data' => $partys));

	}

}