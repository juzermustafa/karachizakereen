<?php

ini_set("auto_detect_line_endings", true);

date_default_timezone_set('Asia/Karachi');
set_time_limit(0);

require 'vendor/Slim/Slim.php';
require_once 'vendor/Paris/Validator.php';
require_once 'vendor/Paris/idiorm.php';
require_once 'vendor/Paris/paris.php';
require_once 'vendor/PHPMailer/class.phpmailer.php';
require_once 'tcpdf_config.php';
require_once 'vendor/CHtml.php';
require_once 'lib/Setting.php';

define("DB_HOST", "192.168.0.10");
define("DB_NAME", "zakereen");
define("DB_USER", 'root');
define("DB_PASS", 'root');

ORM::configure('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME);
ORM::configure('username', DB_USER);
ORM::configure('password', DB_PASS);
ORM::configure('return_result_sets', true);

\Slim\Slim::registerAutoloader();

$app = new \Slim\QSlim(array(
    'view' => '\Slim\LayoutView',
    'layout' => 'layout.php'
        ));

$app->siteConfig = function(){
	return new SiteConfig();
};

require_once 'lib/Plinq.php';
require_once 'lib/CommonHelper.php';
require_once 'lib/Breadcrumb.php';
require_once 'lib/HijriCalendar.php';

?>