<!-- Button trigger modal -->
<div class="panel panel-default">
	<div class="panel-body">
		<div class="text-center">
			<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Add New Event</button>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Event</h4>
			</div>
			<div class="modal-body">
				<form action="" class="form" method="post">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<label>Event Title:</label>
							<input type="text" class="form-control required" id="title" value="" name="title" />
						</div>
						<div class="col-sm-6 col-md-6">
							<label>Event Date:</label>
							<input type="text" class="form-control datepicker required" id="event_date" value="" name="event_date" />
						</div>
						<div class="clearfix"></div>
						<br />
						<div class="col-sm-12 col-md-12 text-right">
							<button type="submit" class="search-submit btn btn-primary">Save</button>
							<div class="clearfix"></div>
							<span class="loading-search">&nbsp;</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>
<p></p>
<?php if(!empty($events)) { ?>
	<div class="table-responsive">
		<table class="table table-striped table-bordered datatable">
			<thead>
				<tr>
					<th class="text-center">S.No</th>
					<th>Title</th>
					<th class="text-center">Date</th>
					<th class="text-center">Hijri Date</th>
					<th class="text-center"></th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach ($events as $event) {
					$source = $event['event_date'];
					$date = new DateTime($source); ?>
					<tr>
						<td class="valign text-center"><?php echo $i++; ?></td>
						<td class="valign"><?php echo $event['title']; ?></td>
						<td class="valign text-center"><?php echo $date->format('d-m-Y'); ?></td>
						<td class="valign text-center"><?php echo $hijri_date->date($event['event_date'], +1)->getFullDate(); ?></td>
						<td class="valign text-center"><a href="<?php echo HTTP_SERVER; ?>zakereen?id=<?php echo $event['id']; ?>" class="button">Show Schedule</a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php } else { ?>
	<div class="alert alert-danger" role="alert">No Events ..</div>
<?php } ?>