<?php if (isset($flash['success'])) : ?>
    <div class="alert alert-success"><?php echo $flash['success']; ?></div>
<?php endif; ?>
<?php if (isset($flash['errors'])) : ?>
    <div class="alert alert-danger"><?php echo $flash['errors']; ?></div>
<?php endif; ?>
<?php if(!isset($aResults) && isset($aParties)): ?>
    
    <div class="panel panel-default form-horizontal">
        <div class="panel-heading">
            <h3 class="panel-title"><span class="glyphicon glyphicon-filter"></span> Filter</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <form class="form-horizontal" id="schedule_report">
                    <div class="col-sm-4 col-md-5">
                        <label>Event Name: </label>
                        <?php $options = array('class' => 'form-control','prompt' => 'Choose Event'); ?>
                            <?php if($event_id != '') {
                                $options['disabled'] = true;
                            }
                        ?>
                        <?php echo CHtml::dropDownList('event_id',$event_id,$aEvents, $options); ?>
                    </div>
                    <div class="col-sm-4 col-md-5">
                        <label>Party Name: </label>
                        <?php echo CHtml::dropDownList('party','',$aParties, array('class' => 'form-control chzn-select','prompt' => 'Choose Party')); ?>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <label>&nbsp;</label>
                        <button type="button" style="width: 100%;" class="btn btn-primary" id="submit"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form id="schedule_form" method="post" action="" class="form hide">
        <div class="panel panel-default form-horizontal">
            <div class="panel-body">
                <div class="text-right">
                    <input type="hidden" name="event_id" value="<?php echo $event_id; ?>" />
                    <button type="button" name="submit" id="schedule_submit" class="btn btn-success" value="create">Save</button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="schedule_table">
                <thead>
                    <tr>
                        <th class="text-center">Urus / Majlis Title</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Mohalla</th>
                        <th class="text-center">Multiple?</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" class="form-control urus" name="target[0][urus]" />
                        </td>
                        <td>
                            <input type="text" class="form-control date datepicker" data-date-format="yyyy-mm-dd" data-provide="datepicker" name="target[0][date]" />
                        </td>
                        <td>
                        	<?php echo CHtml::dropDownList('target[0][mohalla]','',$aMohalla, array('class' => 'form-control mohalla','prompt' => 'Choose Mohalla')); ?>
                        </td>
                        <td class="valign text-center">
                            <input type="checkbox" class="multiple" value="1" name="target[0][is_multiple]" />
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-md btn-primary remove_schedule">Remove</button>
                        </td>
                    </tr>
                </tbody>
                <tfoot class="hidden">
                    <tr>
                        <td>
                            <input type="text" class="form-control urus" name="target[-1][urus]" />
                        </td>
                        <td>
                            <input type="text" class="form-control date datepicker" data-date-format="yyyy-mm-dd" data-provide="datepicker" name="target[-1][date]" />
                        </td>
                        <td>
                        	<?php echo CHtml::dropDownList('target[-1][mohalla]','',$aMohalla, array('class' => 'form-control mohalla','prompt' => 'Choose Mohalla')); ?>
                        </td>
                        <td class="valign text-center">
                            <input type="checkbox" class="multiple" name="target[-1][is_multiple]" />
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-md btn-primary remove_schedule">Remove</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="panel panel-default form-horizontal">
            <div class="panel-body">
                <div class="text-center">
                    <button type="button" class="btn btn-info" id="add_schedule">Add New Row</button>
                </div>
            </div>
        </div>
    </form>
<?php endif; ?>  