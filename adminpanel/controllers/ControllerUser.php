<?php
class ControllerUser extends BaseController
{
    public function login() {
        if(isset($_SESSION['user_id'])){
            session_unset();
        }
        $req = $this->app->request();
        $data = $req->post();
        $oModel = Model::factory('User')->create();
        $aResult = $oModel->authenticate($data);
        
        if (!isset($aResult['error'])) {
            $this->app->redirect(CURR_DIR .'event');
        } else {
            $this->app->flash('errors', $aResult['error']);
            $this->app->redirect(CURR_DIR . 'login' );
        }
    }
        
    public function loginForm() {
        $this->app->breadcrumb->clearList();
        
        if (isset($_SESSION['user_id'])) {
            $_SESSION['name'] = "test";
            session_unset();
            session_destroy();
        }
        
        //$data['layout'] = false;
        $this->app->render('user/login.php');
    }
    
    public function logout() {
        session_unset();
        session_destroy();
        $this->app->redirect(CURR_DIR);
    }

    public function zakereenIndex() {

        $result = array();
        $this->app->render('zakereen.php', $result);
    }

    public function getDetails() {
        $party = $this->app->request()->params('party', null);
        $event_id = $this->app->request()->params('event_id', null);
        $result['schedules'] = Model::factory('Schedule')
                            ->where('party', $party)
                            ->where('event_id',$event_id)
                            ->find_array();

        echo json_encode($result);
    }

    public function scheduleIndex() {
        $result = array();

        $result['event_id'] = $this->app->request()->params('id', '');

        $result['aParties'] = CHtml::listData(Model::factory('Party')->order_by_asc('name')->find_array(), 'id', function($row){return 'Hizbe '.$row['name'];});
        $result['aTeepers'] = CHtml::listData(Model::factory('Party')->select('id')->select_expr('CONCAT(teeper," - (",IFNULL(name,"N/A"),")")','party')->order_by_asc('teeper')->find_many(), 'id', 'party');
        $result['aMohalla'] = CHtml::listData(Model::factory('Mohalla')->order_by_asc('name')->find_many(), 'id', 'name');
        $result['aEvents'] = CHtml::listData(Model::factory('Event')->order_by_desc('event_date')->where('is_deleted',0)->find_many(), 'id', 'title');
        $this->app->render('zakereen.php', $result);
    }    

    public function addSchedule() {

        $aResult = array();
        $aUpdatedTargets = array();
        try {
            if ($this->app->request()->post()) {
                ORM::get_db()->beginTransaction();
                
                $aTargets = $this->request->post('target');
                $party = $this->request->post('party');
                $event_id = $this->request->post('event_id');
                $aListTargets = CHtml::listData(Model::factory('Schedule')->where('party', $party)->where('event_id', $event_id)->find_many(), 'id', 'id');

                foreach ($aTargets as $i=>$aTarget) {
                    if($i == -1)
                        continue;
                    
                    $oModelTarget = Model::factory('Schedule')->create();
                    
                    if(isset($aTarget['id']) && $aTarget['id'] != '') {
                        $oModelTarget = Model::factory('Schedule')->where('id', $aTarget['id'])->find_one();
                        $aUpdatedTargets[] = $aTarget['id'];
                        unset($aTarget['id']);
                    }
                    
                    $is_multiple = isset($aTarget['is_multiple']) ? 1 : 0;
                    
                    $date = $aTarget['date'];
                    $aTarget['date'] = date('Y-m-d', strtotime($date));
                    $oModelTarget->setFields($aTarget);
                    $oModelTarget->is_multiple = $is_multiple;
                    $oModelTarget->party = $party;
                    $oModelTarget->event_id = $event_id;
                    $oModelTarget->save();
                    if($oModelTarget->hasErrors()) {
                        throw new Exception(CHtml::errorSummary($oModelTarget));
                    }
                }
                if (!empty($aListTargets) || !empty($aUpdatedTargets)) {
                        $aDiff = array_diff($aListTargets, $aUpdatedTargets);
                        if (!empty($aDiff)) {
                            foreach ($aDiff as $iId) {
                                Model::factory('Schedule')->find_one($iId)->delete();
                            }
                        }
                    }
            }
            ORM::get_db()->commit();
            $this->app->flash('success', "Successfully Saved");
        }
        catch (Exception $ex) {
            ORM::get_db()->rollBack();
            $this->app->flash('errors', $e->getMessage());
        }
        $aResult['result'] = CURR_DIR . 'event';
        echo json_encode($aResult);
    }   

    public function event() {
        $hijri = new HijriCalendar();
        $events = Model::factory('Event')->find_array();
        $this->app->render('event.php', array('events' => $events, 'hijri_date' => $hijri));
    }

    public function eventSave() {
        $params = $this->app->request()->post();
        $oEvent = Model::factory('Event')->create();
        $oEvent->setFields($params);
        $oEvent->save();

        $this->app->redirect(CURR_DIR . 'event');

    } 

}

?>