<?php

$count = $pages = 0;

/* * ****************************** */
/* * *****Start Login Page********* */
/* * ****************************** */
$app->get('/', 'ControllerUser:loginForm');
$app->get('/login', 'ControllerUser:loginForm');
$app->get('/logout', 'ControllerUser:logout');

$app->post('/', 'ControllerUser:login');
$app->post('/login', 'ControllerUser:login');

$app->get('/zakereen', $authenticate($app), 'ControllerUser:scheduleIndex');
$app->post('/zakereen', 'ControllerUser:addSchedule');

$app->post('/schedule', 'ControllerUser:getDetails');

$app->get('/event', $authenticate($app), 'ControllerUser:event');
$app->post('/event', 'ControllerUser:eventSave');
?>