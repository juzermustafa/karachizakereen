$(document).on('keydown', 'input.search-text', function(e) {
    if (e.keyCode == 13) {
        $('.search-submit').trigger('click');
    }
});

$(document).on('click', '.search-submit', function(){
    var search_value = $('.search-text').val();
    $('.error').hide();
    if(search_value == '' || isNaN(search_value) || search_value.length != 8 ){
        $('.error').fadeIn();
        setTimeout(function(){
	        $('.error').fadeOut();
	    }, 1500);
        return;
    }
    $.ajax({
        type: 'get',
        url: baseUrl+'search',
        data: {search_term:search_value},
        beforeSend: function(){
            $(".loading-search").show();
        },
        success: function(res){
            $(".loading-search").hide();
            $('#surat_data').hide();
            $('#search-data').show();
            $('#search-data').html(res);
        }
    });
});

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

$(document).on('click', '#schedule_submit', function(e) {
    e.preventDefault();
    $('.ajax-result, .alert').remove();
    var obj = $(this);
    var error = $('<div class="ajax-result alert alert-danger"></div>');
    var bError = false;
    /*if ($('#schedule_table tbody tr').length == 0) {
        error.html("Please enter a schedule first");
        bError = true;
    }*/
    
    $('#schedule_table input, select').not(':checkbox,:hidden').each(function() {
        if($(this).val() == '') {
            error.html("Incomplete Schedule Entered");
            bError = true;
        }
    });
    if(bError){
        $('.panel-heading').before(error);
        return true;
    }
    var form = $('#schedule_form').serializeArray();
    form.push({name: 'party', value: $('#party').val()});
    form.push({name: 'event_id', value: $('#event_id').val()});
    $.ajax({
        url: baseUrl + 'zakereen',
        type: 'POST',
        dataType: 'json',
        data: form,
        beforeSend: function() {
            //obj.hide();
            obj.before('<div class="loading-search"></div>');
            $('.ajax-result').remove();
        },
        success: function(res) {
            location.reload();
            // location.href = res.result;
        }
    });
});

function addScheduleRow() {
    var html = $('#schedule_table tfoot.hidden').html();
    var count = $('#schedule_table tbody tr').length;
    html = html.replace(/\[-1\]/g, '[' + count + ']');
    $('#schedule_table tbody').append(html);
    $('#remove_schedule').removeClass('hide');
    /*if ($('#schedule_table tbody tr').length > 1) {
    }*/
    return $('#schedule_table tbody tr:last');
}

$(document).on('click', '#remove_schedule', function() {
        $(this).closest('tr').remove();
    // if ($('#schedule_table tbody tr').length > 1)
});

$(document).on('click', '#add_schedule', function() {
    addScheduleRow();
});

function clearScheduleForm() {
    $('#schedule_form input').val('');
    $('#schedule_form input[type=hidden]').remove();
    $('#schedule_form select option:first').attr('selected', true);
    $('#schedule_table tbody tr').not(':first').remove();
    $('#remove_schedule').addClass('hide');
}

$(document).on('click', '#schedule_report #submit', function() {
    var obj = $(this);
    $('.ajax-result').remove();
    if($('#party').val() == "") {
        $('#schedule_form').addClass('hide');
        return false;
    }
    $.ajax({
        url: baseUrl + 'schedule',
        type: 'POST',
        dataType: 'json',
        data: {party: $('#party').val(), event_id: $('#event_id').val()},
        beforeSend: function() {
            $('#schedule_form').addClass('hide');
            //obj.hide();
            obj.before('<div class="loading-search"></div>');
            $('.alert-danger').remove();
        },
        success: function(res) {
                clearScheduleForm();
                var html = "";

                $('#schedule_form').removeClass('hide');
                if (res.schedules.length > 0) {
                    $.each(res.schedules, function (i, val) {
                        if (i == 0 && $('#schedule_table tbody tr').length >= 1) {
                            var row = $('#schedule_table tbody tr:first');
                        } else {
                            var row = addScheduleRow();
                        }
                        row.append('<input type="hidden" name="target[' + i + '][id]" value="' + val.id + '" />');
                        $('.mohalla option[value='+val.mohalla+']', row).attr('selected', true);
                        $('.urus', row).val(val.urus);
                        $('.multiple', row).prop('checked',val.is_multiple == 1);
                        $('.date', row).val(val.date);
                    });
                    // $('.party').prop('disabled',true);
                } else {
                    if($('#schedule_table tbody tr').length == 0)
                        addScheduleRow();
                    
                    alert('No Record Found');
                }
                    
                $('#schedule_submit').val($('#party').val());
        },
        fail: function() {
            alert('Error: Saving data');
        },
        complete: function() {
            $('.loading-search').remove();
            obj.show();
        }
    });
});

$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});

$(document).on('click', '.attended', function() {
    var obj = $(this);
    var checked = obj.prop('checked') == true ? "1" : "0";
    $.ajax({
        url: baseUrl + 'attended',
        type: 'POST',
        dataType: 'json',
        data: {id: obj.attr('data-id'), type: obj.attr('data-type'), status: checked},
        beforeSend: function() {
            obj.hide();
            obj.before('<div class="loading-search"></div>');
            $('.ajax-result').remove();
        },
        success: function(res) {
            if (typeof res.error != 'undefined') {
                var error = $('<div class="ajax-result alert alert-danger" style="display: none;"></div>');
                error.html(res.error).fadeIn();
                $('#search-data .table-responsive').before(error);
            } else {
                var success = $('<div class="ajax-result alert alert-success" style="display: none;"></div>');
                success.html(res.success).fadeIn();
                $('#search-data .table-responsive').before(success);
            }
        },
        complete: function() {
            $('.loading-search').remove();
            obj.show();
        }
    });
    setTimeout(function(){
        $('.alert').fadeOut();
    }, 3000)
});

$(document).on('change', '.verified', function() {
    var obj = $(this);
    var checked = obj.val();
    $.ajax({
        url: baseUrl + 'attended',
        type: 'POST',
        dataType: 'json',
        data: {id: obj.attr('data-id'), type: obj.attr('data-type'), status: checked},
        beforeSend: function() {
            obj.hide();
            obj.before('<div class="loading-search"></div>');
            $('.ajax-result').remove();
        },
        success: function(res) {
            if (typeof res.error != 'undefined') {
                var error = $('<div class="ajax-result alert alert-danger" style="display: none;"></div>');
                error.html(res.error).fadeIn();
                $('#search-data .table-responsive').before(error);
            } else {
                var success = $('<div class="ajax-result alert alert-success" style="display: none;"></div>');
                success.html(res.success).fadeIn();
                $('#search-data .table-responsive').before(success);
            }
        },
        complete: function() {
            $('.loading-search').remove();
            obj.show();
        }
    });
    setTimeout(function(){
        $('.alert').fadeOut();
    }, 3000)
});

$(document).on('click', '.search-mohalla', function(){
    var search_value = $('#mohalla').val();
    $('.error').hide();
    if(search_value == ''){
        $('.error').show();
        return;
    }
    $.ajax({
        type: 'POST',
        url: baseUrl+'mohalla',
        data: {search_term:search_value},
        dataType: 'html',
        beforeSend: function(){
            $(".loading-search").show();
        },
        success: function(res){
            $(".loading-search").hide();
            $('#search-data').html(res);
        }
    });
});