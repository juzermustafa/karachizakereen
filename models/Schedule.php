<?php

class Schedule extends Model{

	public static $table = 'schedule';

    protected $_fields = array(
        'id',
        'party',
        'date',
        'urus',
        'mohalla',
        'is_multiple'
    );

}

?>